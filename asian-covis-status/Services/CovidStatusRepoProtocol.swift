//
//  CovidStatusRepoProtocol.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation

typealias ResultCompletionHandler = (_ apiresponse : [AsiaCovidModel]?, _ error : APIError?) -> Void

protocol CovidStatusRepoProtocol {
    
    func getCovidStatus(completion : @escaping ResultCompletionHandler)
}
