//
//  CovidStatusRepo.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation

enum APIError : String, Error {
    case noInternet = "Please check internet connection"
    case notFound = "No data found or page removed"
}

class CovidStatusRepo: CovidStatusRepoProtocol {
    
    static let instance = CovidStatusRepo()
    
    lazy var networkManager : NetworkManager = {
        return NetworkManager()
    }()
    
    func getCovidStatus(completion: @escaping ResultCompletionHandler) {
        let target : NetworkEndPoint = .getCovidStatus
        networkManager.makeApiCall(target, completion: completion)
    }
}
