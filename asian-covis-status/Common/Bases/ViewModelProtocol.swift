//
//  BaseViewModel.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation



protocol ViewModelProtocol {
    
    
    associatedtype cellViewModel
    
    var observState : Observable<State?>? { get set }
    
    var apiProtocol : CovidStatusRepoProtocol? { set get}
   
    func getCellViewModel(at indexPath : IndexPath) -> cellViewModel?
}

extension ViewModelProtocol {
    func getCellViewModel(at indexPath : IndexPath) -> cellViewModel? { return nil }
    
}

