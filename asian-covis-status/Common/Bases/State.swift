//
//  State.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation
public enum State  {
    case empty
    case loading
    case success
    case reloadData
    case faliure(String?)
}
