//
//  NetworkEndPoint.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum Environment {
    case development
}

enum NetworkEndPoint {
    
    case getCovidStatus
}


extension NetworkEndPoint : TargetType {
 
    
    private var environmentUrl : String {
        switch NetworkManager.environment {
        case .development:
            return "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/"
        }
    }
    
     public var baseURL: URL {
          guard let url = URL(string: environmentUrl) else {
              fatalError("BaseURL can't be configured")
          }
          
          return url
      }
    
    public var path : String {
        switch self {
        case .getCovidStatus:
            return "npm-covid-data/asia"
        }
        
        
    }
    
    
    public var method: Alamofire.HTTPMethod {
        switch self {
        case .getCovidStatus:
            return .get
        }
    }
    
    
    public var task : Task {
        switch self {
        case .getCovidStatus:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
            
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
           switch self {
           case .getCovidStatus:
                   return URLEncoding.default
           }
       }
    
    public var headers: [String : String]? {
        switch self {
        case .getCovidStatus:
            return ["content-type": "application/json; charset=utf-8",
                    "x-rapidapi-key":"8d7bd54359msh906fc15f42b2c36p1100f7jsn3ed382455485"]
        }
    }
    
    public var sampleData: Data {
        switch self {
        default:
            return Data()
            
        }
    }
}

