//
//  CovidStatusCell.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import UIKit
import Kingfisher

class CovidStatusCell: UITableViewCell {

  @IBOutlet weak var countrytitleLbl: UILabel!
  @IBOutlet weak var totalRecoverdLbl: UILabel!
    @IBOutlet weak var confirmedCasesLbl: UILabel!
    @IBOutlet weak var activeCasesLbl: UILabel!
    
    @IBOutlet weak var deathCasesLbl: UILabel!
    var statusCellViewModel : StatusCellViewModel? {
        didSet{
            countrytitleLbl.text = statusCellViewModel?.countryTitlelbl
            totalRecoverdLbl.text = statusCellViewModel?.recoveredValuelbl
            confirmedCasesLbl.text = statusCellViewModel?.confirmedValuelbl
            
            activeCasesLbl.text = statusCellViewModel?.activeValuelbl
            
            deathCasesLbl.text = statusCellViewModel?.deathValuelbl
        }
    }
    
}
