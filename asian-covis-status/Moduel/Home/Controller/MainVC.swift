//
//  ViewController.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import UIKit
import JGProgressHUD

class MainVC: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var initialPage = 1
    
    lazy var progressIndicator = {
        return JGProgressHUD(style: .dark)
    }()
    
     lazy var mViewModel : HomeViewModel =  {
            return HomeViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initVM()
        
        mViewModel.getAsianStatus()
    }
    
    
    private func initVM() {
      
        
        mViewModel.observState?.bind { [weak self] (state) in
            guard let self = self else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch state {
                case .loading:
                    self.progressIndicator.show(in: self.view)
                    
                case .reloadData:
                    self.tableView.reloadData()
                    
                case .faliure(let errorMessage):
                    self.progressIndicator.dismiss()
                    print("error found is: \(String(describing: errorMessage))")
                default:
                    self.progressIndicator.dismiss()
                }
            }
            
        }
    }
    
    
    private func initView() {
        let nib = UINib(nibName: "\(CovidStatusCell.self)", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "\(CovidStatusCell.self)")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
     

  

}






