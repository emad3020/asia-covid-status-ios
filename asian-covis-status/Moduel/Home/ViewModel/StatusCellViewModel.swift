//
//  StatusCellViewModel.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation
import Kingfisher

struct StatusCellViewModel {
    let countryTitlelbl: String
    let recoveredValuelbl : String
    let confirmedValuelbl : String
    let activeValuelbl : String
    let deathValuelbl : String
    
}
