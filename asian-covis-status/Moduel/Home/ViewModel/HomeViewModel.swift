//
//  HomeViewModel.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation
import Kingfisher

class HomeViewModel: ViewModelProtocol {
    
    
    var observState: Observable<State?>?
    
    typealias T = AsiaCovidModel?
    typealias cellViewModel  = StatusCellViewModel
    
    var observer: Observable<AsiaCovidModel?>?
    var apiProtocol: CovidStatusRepoProtocol?
    
    var statusArray = [AsiaCovidModel]()
    
    private var cellViewModels  = [StatusCellViewModel]() {
        didSet{
            self.observState?.value = .reloadData
        }
    }
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    
    init(apiService : CovidStatusRepoProtocol = CovidStatusRepo()) {
        self.apiProtocol = apiService
        self.observer = Observable<AsiaCovidModel?>(nil)
        self.observState = Observable<State?>(.empty)
    }
    
 
    
    func getCellViewModel(at indexPath: IndexPath) -> StatusCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func getAsianStatus(){
        apiProtocol?.getCovidStatus(completion: { [weak self] (statusResponse, error) in
            guard let self = self else { return }
            
            guard error == nil,let response = statusResponse else {
                self.observState?.value = .faliure(error?.rawValue)
                return
            }
            self.fetchCovidStatusData(statusData: response)
            self.observState?.value = .success
        })
    }
    
    
    private func fetchCovidStatusData(statusData : [AsiaCovidModel]) {
        self.statusArray = statusData
        
        var cellVM = [StatusCellViewModel]()
        
        for statusItem in statusData {
            cellVM.append(creareCellViewModel(item: statusItem))
        }
        
        self.cellViewModels.append(contentsOf: cellVM)
    }
    
    private func creareCellViewModel(item : AsiaCovidModel) -> StatusCellViewModel {
        return StatusCellViewModel.init(countryTitlelbl: item.country, recoveredValuelbl: item.totalRecovered, confirmedValuelbl: "\(item.totalCases)", activeValuelbl: "\(item.activeCases)", deathValuelbl: "\(item.totalDeaths)")
    }
    
    
    
    
}
