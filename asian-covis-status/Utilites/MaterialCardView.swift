//
//  MaterialCardView.swift
//  asian-covid-status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation
import UIKit

private var isMaterialView = false
private var backColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
private var cornerRadiusValue: CGFloat = 0

extension UIView {
    
    @IBInspectable var bgroundColor: UIColor {
        set {
            self.backgroundColor = newValue
        }
        
        get {
            
            return backColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            cornerRadiusValue = newValue
            setUpView()
        }
        get {
            return cornerRadiusValue
        }
    }
    
    @IBInspectable var MaterialDesign : Bool {
        
        set {
            
            isMaterialView = newValue
            
            setUpView()
        }
        
        
        get {
            return isMaterialView
        }
    }
    
    fileprivate func setUpView () {
        
        if isMaterialView {
            
            self.layer.masksToBounds = false
            self.layer.cornerRadius = cornerRadiusValue
            self.layer.shadowOpacity = 0.8
            self.layer.shadowRadius = 3.0
            self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            self.layer.shadowColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0).cgColor
            
        } else {
            
            self.layer.cornerRadius = 0
            self.layer.shadowOpacity = 0
            self.layer.shadowRadius = 0
            self.layer.shadowColor = nil
        }
        
    }
    
    open override func prepareForInterfaceBuilder() {
        
        if isMaterialView {
            
            setUpView()
            
        }
    }
}
