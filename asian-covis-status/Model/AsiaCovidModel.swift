//
//  AsiaCovidModel.swift
//  Asian Covis Status
//
//  Copyright © 2021 Askerlap. All rights reserved.
//

import Foundation

// MARK: - AsiaCovidModelElement
struct AsiaCovidModel: Codable {
    let id: String
        let country: String
        let twoLetterSymbol, threeLetterSymbol: String
        let infectionRisk, caseFatalityRate, testPercentage, recoveryProporation: Double
        let totalCases, newCases, totalDeaths, newDeaths: Int
        let totalRecovered: String
        let newRecovered, activeCases: Int
        let totalTests, population: String
        let oneCaseeveryXPpl, oneDeatheveryXPpl, oneTesteveryXPpl: Int
        let deaths1MPop: Double
        let seriousCritical, tests1MPop, totCases1MPop: Int
    
    enum CodingKeys: String, CodingKey {
          case id
          case country = "Country"
          case twoLetterSymbol = "TwoLetterSymbol"
          case threeLetterSymbol = "ThreeLetterSymbol"
          case infectionRisk = "Infection_Risk"
          case caseFatalityRate = "Case_Fatality_Rate"
          case testPercentage = "Test_Percentage"
          case recoveryProporation = "Recovery_Proporation"
          case totalCases = "TotalCases"
          case newCases = "NewCases"
          case totalDeaths = "TotalDeaths"
          case newDeaths = "NewDeaths"
          case totalRecovered = "TotalRecovered"
          case newRecovered = "NewRecovered"
          case activeCases = "ActiveCases"
          case totalTests = "TotalTests"
          case population = "Population"
          case oneCaseeveryXPpl = "one_Caseevery_X_ppl"
          case oneDeatheveryXPpl = "one_Deathevery_X_ppl"
          case oneTesteveryXPpl = "one_Testevery_X_ppl"
          case deaths1MPop = "Deaths_1M_pop"
          case seriousCritical = "Serious_Critical"
          case tests1MPop = "Tests_1M_Pop"
          case totCases1MPop = "TotCases_1M_Pop"
      }
}
